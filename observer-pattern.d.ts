export interface IObservable<T> {
    observers: IObserver<T>[];
    addObserver(observer: IObserver<T>): void;
    removeObserver(observer: IObserver<T>): void;
    notifyObservers(data: T): void;
}
export interface IObserver<T> {
    onNotifiedByObservable(data: T): void;
}
